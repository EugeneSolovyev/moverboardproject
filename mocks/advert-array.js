export const ADVERT_ARRAY = [
	{
		key: 1,
		user: {
			name: "John Doe",
			company: "Super Drive LLC",
			avatar: "http://www.logiccircuit.org/forum/download/file.php?avatar=980_1261893092.jpg"
		},
		transportation: {
			from: 'Boston',
			to: 'San Jose',
			date: 'Jan 03, 2018',
			advertImage: 'https://www.daimler.com/bilder/konzern/geschaeftsfelder/daimler-trucks/bildergalerie-2/freightliner-inspiration-truck-w768xh432-cutout.jpg'
		},
		additionalText: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
						Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
						Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.`
	},
	{
		key: 2,
		user: {
			name: "Michel Jackson",
			company: "Super Drive LLC",
			avatar: "https://www.thefamouspeople.com/profiles/images/michael-jackson-3.jpg"
		},
		transportation: {
			from: 'New York',
			to: 'San Francisco',
			date: 'Mar 03, 2018',
			advertImage: 'http://tigres.com.ua/-/uploads/ru/00/00/06/54/big-super-truck-gruzovik-v-pie-up--1392629782.jpg'
		},
		additionalText: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
						Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
						Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.`
	},
];
