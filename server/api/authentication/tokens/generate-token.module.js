const jws = require('jwt-simple');
const cryptoRandomString = require('crypto-random-string');

module.exports = {
	register: function (hash) {
		let salt = cryptoRandomString(10);
		let secret = Buffer.from(salt, 'hex');
		let token = jws.encode({
			pass: hash,
		}, secret);
		return token;
	},
};
