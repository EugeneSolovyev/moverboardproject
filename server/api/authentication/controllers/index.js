const passport = require('passport');
const USER_MODEL = require('../../schema/User/user-schema.js').users;
const auth = require('../models/user.model');
const bcrypt = require('bcrypt-nodejs');

function login(req, res, next) {
	passport.authenticate('local',
		function (err, user, info) {
			return err
				? next(err)
				: user
					? req.logIn(user, function (err) {
						return err
							? next(err)
							: res.send({status: "OK", data: user});
					})
					: res.send({status: "ERROR", data: "User not exist"});
		}
	)(req, res, next);
}

function register(req, res, next) {
	let tempUser = {};
	auth.login_exist(req.body.login, afterCheckForExist);
	function afterCheckForExist(err, resolve) {
		if (resolve !== null)
			res.status(500).json({errorMessage: 'User with same login already exist'});
		else
			bcrypt.hash(req.body.password, null, null, afterCrypt);
	}
	
	function afterCrypt(err, hash) {
		if (err)
			res.status(500).json({errorMessage: err});
		else new USER_MODEL({
			login: req.body.login,
			hash: hash,
			googleID: '',
			role: req.body.role
		}).save(afterSave);
	}
	
	function afterSave(err, NewUser) {
		if (err)
			next(err);
		else
			tempUser = NewUser;
			req.logIn(NewUser, afterLogin);
	}
	
	function afterLogin(err) {
		if (err)
			next(err);
		else
			res.send({status: "OK", data: tempUser});
	}
}

function logout(req, res) {
	console.log('get /logout'.red);
	req.logout();
	res.redirect('/');
}

function google(req, res, next) {
	passport.authenticate('google')(req, res, (err, something) => {
		console.log("user info:".blue);
		console.log(req.user);
		console.log("sessionID:".blue);
		console.log(req.sessionID);
		res.redirect("/profile?needGetProfile");
	});
}

module.exports = {
	login: login,
	register: register,
	logout: logout,
	google: google
};
