const passport = require('passport');
const googleStrategy = require('passport-google-oauth20');
const LocalStrategy = require('passport-local').Strategy;
const keys = require('./keys');
const USER_MODEL = require('../schema/User/user-schema.js').users;
const auth = require('./models/user.model');
const generateToken = require('./tokens/generate-token.module.js');
const bcrypt = require('bcrypt-nodejs');

passport.serializeUser((user, done) => {
	done(null, user.login);
});

passport.deserializeUser((login, done) => {
	auth.login_exist(login, (err, user) => {
		done(null, user);
	});
});

passport.use(
	new googleStrategy(
		{
			callbackURL: "/auth/google/redirect",
			clientID: keys.google.clientID,
			clientSecret: keys.google.clientSecret
		},
		(accessToken, refreshToken, profile, done) => {
			
			auth.googleID_exist(profile.id, cb);//identify
			function cb(err, user) {
				if (user == null) { //need to create new user
					new USER_MODEL({
						login: profile.emails[0].value,
						hash: generateToken.register(profile.id),
						googleID: profile.id,
						role: 'user'
					}).save().then(NewUser => {
						console.log("new user created:".blue);
						console.log(NewUser);
						done(err, NewUser);
					});
				} else { // use existing user
					console.log("user found:".blue);
					done(err, user);
				}
			}
		}
	)
);

passport.use(
	new LocalStrategy(
		{
			usernameField: 'login',
			passwordField: 'password'
		},
		(login, password, done) => {
			auth.login_exist(login, (err, user) => {
				return err
					? done(err)
					: user
						? bcrypt.compareSync(password, user.hash)
							? done(null, user)
							: done(null, false, {message: 'Incorrect password.'})
						: done(null, false, {message: 'Incorrect username.'});
			});
		}
	)
);
