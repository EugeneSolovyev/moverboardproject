const passport = require('passport');

function configApp(app) {
	app.use(passport.initialize());
	app.use(passport.session());
	
	require('./passportSetup');
	
	const authRoutes = require('./routes.js');
	app.use('/auth', authRoutes);
}

module.exports = configApp;
