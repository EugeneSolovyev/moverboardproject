const passport = require('passport');
const path = require('path');
const router = require('express').Router();
const authControllers = require('./controllers/index');

// Auth system
router.post('/login', authControllers.login);
router.post('/register', authControllers.register);
router.get('/logout', authControllers.logout);
router.get('/google', passport.authenticate('google', {
	scope: [
		'profile',
		'email'
	]
}));
router.get('/google/redirect', authControllers.google);

//router.get('/google/redirect', passport.authenticate('google'), );


module.exports = router;
