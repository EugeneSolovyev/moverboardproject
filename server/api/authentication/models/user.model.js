const UsersModel = require('../../schema/User/user-schema').users;

exports.login_exist = (login, callback) => {
	UsersModel.findOne({login: login}, (err, docs) => {
		callback(err, docs);
	})
};

exports.googleID_exist = (googleID, callback) => {
	UsersModel.findOne({googleID: googleID}, (err, docs) => {
		callback(err, docs);
	})
};
