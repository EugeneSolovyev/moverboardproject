const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const MongoStore = require('connect-mongo')(session);
const db = require('../../../config/db-url').url;
const moment = require('moment');

exports.session_start = (app) => {
	const MAX_AGE = 86400;
	const EXPIRES = moment().add(1, 'days').unix();
	// Middlewares, которые должны быть определены до passport:
	app.use(cookieParser());
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(bodyParser.json());
	app.use(session({
		secret: '8ABA5E297BA0F371DBAA0E320D08F52F7D398AF969CB0329F67A85418CB6ACE3',
		resave: false,
		saveUninitialized: false,
		cookie: {
			secure: false,
			maxAge: MAX_AGE,
			expires: EXPIRES
		},
		store: new MongoStore({
			url: db,
		})
	}))
};
