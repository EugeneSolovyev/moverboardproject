let UserModel = require("../schema/User/user-schema").users;

function filterProfile (profile) {
	let filteredProfile = {
		login: profile.login,
		googleID: profile.googleID || "",
		birthday: profile.birthday || "",
		firstName: profile.firstName || "",
		lastName: profile.lastName || "",
		licenseNumber: profile.licenseNumber || "",
		additionalInfo: profile.additionalInfo || "",
		photoUrl: profile.photoUrl || "",
	};
	return filteredProfile;
}

function getProfile(req, res) {
	console.log('запрос post /get_profile'.blue);
	if (req.isAuthenticated()) {
		console.log(`sending profile for ${req.user.login}`.yellow);
		res.send(filterProfile(req.user));
	} else {
		console.log("undefined user tried to see profile".red);
		res.send({status_code:555, status_message: "Unauthorized user"});
	}
}

function saveProfile(req, res) {
	console.log('запрос post /save_profile'.blue);
	if (req.isAuthenticated()) {
		UserModel.findOne({login: req.user.login}, (err, user) => {
			if (err) {
                res.send({status_code: 404, status_message: "not found in db"});
			} else {
                user.set(req.body);
                user.save(function (err, updatedUser) {
                    if (err) {
                        res.send({status_code: 404, status_message: "not saved in db"});
					} else {
                        res.send({status_code: 200, status_message: "saved"});
                    }
                });
			}
		});
		console.log(`profile saved for ${req.user.login}`.yellow);
	} else {
		console.log("undefined user tried to save profile".red);
		res.send({status_code:555, status_message: "Unauthorized user"});
	}
}


module.exports.getProfile = getProfile;
module.exports.saveProfile = saveProfile;
