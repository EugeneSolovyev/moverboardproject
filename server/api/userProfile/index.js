const passport = require("passport");
const ctrl = require('./controllers');

module.exports = (app) => {
	app.get('/get_profile', ctrl.getProfile);
	app.post('/save_profile', ctrl.saveProfile);
};
