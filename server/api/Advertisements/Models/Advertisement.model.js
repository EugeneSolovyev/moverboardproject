const AdvertisementModel = require('../../schema/Advertisement/Advertisement-schema').advertisement;
const fs = require('fs');

exports.get_advertisement_all = (callback) => {
	AdvertisementModel.find({}, (err, docs) => {
		callback(err, docs);
	})
};

exports.get_advertisement = (ID, callback) => {
	AdvertisementModel.findOne()
};

exports.create_advertisement = (data, callback) => {
	
	new AdvertisementModel({
		user_name: data.userName,
		user_last_name: data.userLastName,
		user_company: data.userCompany,
		user_photo: data.userPhoto,
		transportation_from: data.from,
		transportation_to: data.to,
		transportation_date: data.dates,
		transportation_img: data.transportation_img,
		additional_text: data.additional_text,
	}).save((err, newAdvertisement) => {
		callback(err, newAdvertisement);
	})
};
