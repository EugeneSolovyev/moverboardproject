const AdvertisementModel = require('../Models/Advertisement.model');

module.exports = {
	getAllAdvertisements: getAllAdvertisements,
	postNewAdvertisement: postNewAdvertisement,
	updateAdvertisement: updateAdvertisement,
	deleteAdvertisement: deleteAdvertisement,
};

function getAllAdvertisements(_req, res) {
	AdvertisementModel.get_advertisement_all(onResponse);
	
	function onResponse(err, response) {
		if (err !== null) {
			res.status(500).json({
				errorMessage: 'Server is unavailable now. Try again.'
			});
		} else {
			res.status(200).json({
				data: response.filter(advert => advert.actual === true)
			})
		}
	}
}

function postNewAdvertisement(req, res) {
	AdvertisementModel.create_advertisement(req.body, onResponse);
	
	function onResponse(err, response) {
		if (err !== null) {
			res.status(500).json({
				errorMessage: 'Server is unavailable now. Try again.'
			});
		} else {
			res.status(200).json({
				data: response,
			})
		}
	}
}

function updateAdvertisement() {

}

function deleteAdvertisement() {

}
