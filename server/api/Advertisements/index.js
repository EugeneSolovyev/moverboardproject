const AdvertController = require('./Controllers/Advertisement.controller');

module.exports = (app) => {
	app.get('/get_all_advertisements', AdvertController.getAllAdvertisements);
	app.post('/post_advertisement', AdvertController.postNewAdvertisement);
	app.put('/update_advertisement/:advert_id', AdvertController.updateAdvertisement);
	app.delete('/delete_advertisement/:advert_id', AdvertController.deleteAdvertisement);
};
