const SignUpController = require('../authentication/controllers/sign-up.controller');
const SignInController = require('../authentication/controllers/sign-in.controller');
const LogOutController = require('../authentication/controllers/logout.controller');

module.exports = function (app) {
	app.post('/register', SignUpController.sign_up);
	app.put('/logout', LogOutController.log_out);
	app.post('/login', SignInController.sign_in);
};
