const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var USERS = new Schema({
	login: {
		type: String,
		required: true,
		unique: true,
	},
	hash: {
		type: String,
		required: true,
	},
	googleID: {
		type: String,
		required: false,
	},
	birthday: {
		type: String,
		required: false
	},
	firstName: {
		type: String,
		required: false
	},
	lastName: {
		type: String,
		required: false
	},
	licenseNumber: {
		type: String
	},
	additionalInfo: {
		type: String,
		required: false
	},
	photoUrl: {
		type: String,
		required: false
	},
	role: {
		type: String,
		required: true,
	}
}, {collection: 'users'});

mongoose.model('users', USERS);

module.exports = {
	users: mongoose.model('users'),
};
