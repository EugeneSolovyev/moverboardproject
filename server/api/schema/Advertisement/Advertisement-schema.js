const mongoose = require('mongoose');

const SCHEMA = mongoose.Schema;

const AdvertisementSchema = new SCHEMA({
	user_name: {
		type: String,
		required: true,
		unique: false,
	},
	user_last_name: {
		type: String,
		required: true,
		unique: false,
	},
	user_company: {
		type: String,
		required: true,
		unique: false,
	},
	user_photo: {
		type: String,
		required: false,
		unique: false,
	},
	transportation_from: {
		type: String,
		required: true,
		unique: false,
	},
	transportation_to: {
		type: String,
		required: true,
		unique: false,
	},
	transportation_date: {
		type: String,
		required: true,
		unique: false,
	},
	transportation_img: {
		type: String,
		required: false,
		unique: false,
	},
	additional_text: {
		type: String,
		required: false,
		unique: false,
	},
	actual: {
		type: Boolean,
		default: true,
	}
}, {
	collection: 'advertisement'
});

mongoose.model('advertisement', AdvertisementSchema);

module.exports = {
	advertisement: mongoose.model('advertisement'),
};
