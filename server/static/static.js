const path = require('path');
const express = require('express');
webpackDevHelper = require('./devHelper');

function configApp(app) {
	console.log(process.env.NODE_ENV);
	if (process.env.NODE_ENV == 'development') {
		
		console.log('DEVOLOPMENT ENVIRONMENT: Turning on WebPack Middleware...'.red);
		
		global.index = path.join(global.rootDir, 'src', 'index.html');
		webpackDevHelper.useWebpackMiddleware(app);
		app.get('/*', (req, res) => {
			res.sendFile(global.index);
			console.log(`запрос на ${req.originalUrl}`.blue);
		});
		
	} else {
		console.log('PRODUCTION ENVIRONMENT: only static and api works'.green);
		
		global.index = path.join(global.rootDir, 'build', 'index.html');
		
		app.use(express.static(path.join(global.rootDir, 'build')));
		
		app.get('/*', (req, res, next) => {
			res.sendFile(global.index);
			console.log(`запрос на ${req.originalUrl}`.blue);
		});
	}
}

module.exports = configApp;
