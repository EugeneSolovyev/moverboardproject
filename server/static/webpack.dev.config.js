const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

console.log(path.join(global.rootDir, 'node_modules'));

module.exports = {
	
	cache: true,
	
	entry: {
		index: [
			'webpack-hot-middleware/client?timeout=20000',
			path.join(global.rootDir, 'src', 'index.js')
		]
	},
	
	devtool: 'source-map',
	
	output: {
		filename: 'bundle.js',
		path: path.join(global.rootDir, '__webpack_hmr'),
		publicPath: '/'
	},
	
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			chunks: ['vendor']
		}),
		new HtmlWebpackPlugin({
			inject: true,
			template: path.join(global.rootDir, 'src', 'index.html'),
		}),
		new webpack.HotModuleReplacementPlugin()
	],
	
	resolve: {
		extensions: ['.js', 'map', 'css', 'scss'],
		modules: [path.resolve(global.rootDir, "node_modules")]
	},
	
	module: {
		loaders: [
			{
				test: require.resolve('snapsvg'),
				loader: 'imports-loader?this=>window,fix=>module.exports=0'
			},
			{
				test: /\.(js|jsx|mjs)$/,
				exclude: /(node_modules|bower_components)/,
				
				// "loader" property changed to "loaders" and is now an array!
				loaders: [
					
					{
						loader: 'babel-loader',
						options: {
							plugins: ["react-hot-loader/babel"],
							presets: [
								['es2015', {'modules': false}],
								'react',
								'stage-0',
								'stage-1'
							]
						}
					}
				]
			},
			{
				test: /\.(sass|scss)$/,
				use: [
					{
						loader: "style-loader" // creates style nodes from JS strings
					}, {
						loader: "css-loader" // translates CSS into CommonJS
					}, {
						loader: "sass-loader" // compiles Sass to CSS
					}
				]
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: "style-loader" // creates style nodes from JS strings
					}, {
						loader: "css-loader" // translates CSS into CommonJS
					}
				]
			},
			{
				test: /\.(eot|ttf|woff|woff2)$/,
				use: [
					{loader: 'file-loader'}
				]
			},
			{
				test: /\.svg$/,
				use: [
					{
						loader: 'url-loader'
					}
				]
			}
		]
	}
};
