## To get started, you need
* _npm >=5.6.0_
* _node js (rather >= 8.9.0)_
* _docker_
* _docker-compose_
## To init
```javascript
npm install
docker-compose up -d
npm run www
npm start
```
