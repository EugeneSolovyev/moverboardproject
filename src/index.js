import "./vendor/import"
import "./vendor/normalize.css";
import "react-md/dist/react-md.deep_purple-pink.min.css";
import "simple-line-icons/fonts/Simple-Line-Icons.svg";
import "simple-line-icons/fonts/Simple-Line-Icons.woff2";
import "simple-line-icons/fonts/Simple-Line-Icons.woff";
import "simple-line-icons/fonts/Simple-Line-Icons.ttf";
import "simple-line-icons/fonts/Simple-Line-Icons.eot";
import "simple-line-icons/css/simple-line-icons.css";

import React from "react";
import ReactDOM from "react-dom";
import {Router} from "react-router-dom";
import createHistory from "history/createBrowserHistory"
import {Provider} from "react-redux";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import Store from "./reducers";
import './root.scss';
import App from "./App";

//import { AppContainer } from 'react-hot-loader'

const history = createHistory();

const render = Component => {
	ReactDOM.render (
		<MuiThemeProvider>
			<Provider store={Store}>
				<Router history={history}>
					<Component/>
				</Router>
			</Provider>
		</MuiThemeProvider>, document.getElementById('root')
	)
};

render(App);

if (module.hot) {
	//require('webpack-middleware-hmr/client.js')();
	module.hot.accept('./App', () => {
		render(App)
	})
}
