import React, {Component} from 'react'


export default (ChildComponent) => {
	return class HOCcontainer extends Component {
		
		// here can be placed some functions to test this.props
		validatingProps() {
			return this.props.valid == true;
		}
		
		notValidProps() {
			return (
				<div>
					Something going wrong
				</div>
			);
		}
		
		render() {
			return (
				this.validatingProps() ? <ChildComponent {...this.props}/> : this.notValidProps()
			)
		}
	}
}