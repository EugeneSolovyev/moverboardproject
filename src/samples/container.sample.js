import React from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import ComponentSample from "./component.sample"
import {sampleAction, sampleActionAsync} from "./actions.sample"

class SimpleContainer extends React.Component {
	
	constructor(props) {
		super(props);
		this.props = {
			//something with props from above
		};
		this.callback = this.callback.bind(this); //must be for every funcs
	}
	
	callback(paramParam) {
		this.props.sampleActionAsync();
		console.log("словили колбек c " + paramParam);
	}
	
	render() {
		
		//here can do something with vars before render (like filters)
		let varForView = varInStore.value;
		
		
		return (
			<div class="chat">
				<ComponentSample callback={this.props.sampleAction} param={varInStore}/>
				<ComponentSample callback={this.callback} />
				А печатаем обработанное{varForView}
			</div>
		)
	}
}


const mapStateToProps = (state) => {
	return {
		varInStore: state.someVariable
	}
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		sampleAction: sampleAction,
		sampleActionAsync: sampleActionAsync
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SimpleContainer)