import React from 'react'
import {
	RaisedButton,
	TextField
} from 'material-ui';

export default class SimpleComponent extends React.Component {
	
	constructor(props) {
		// set defaults
		super(props);
		this.props = {
			//something with props from above
		};
		this.filterForParam = this.filterForParam.bind(this); //must be for every funcs
	}
	
	filterForParam(param) {
		return "ParamParam" + param;
	}
	
	render() {
		//Here can call filters for this.props
		let formattedParam = this.filterForParam(this.props.param);
		
		return (
			<div onClick={this.props.callback}>
				нам передали {formattedParam}
			</div>
		)
	}
}