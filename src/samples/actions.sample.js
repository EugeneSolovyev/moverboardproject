import axios from 'axios';
import actionTypes from '../../constants/ActionTypes'
import urles from '../../constants/urles'

export const sampleAction = (state) => {
	return {
		type: actionTypes.LOG_OUT,
		payload: 1234
	};
};

export const sampleActionAsync = (state) => {
	return dispatch => axios.put(`${urles.ServerURL}/logout`, {
		state: state.vars
	}, urles.config).then(() => {
		dispatch({
			type: actionTypes.LOG_OUT,
			payload: null
		})
	}, rej => {
		console.log(rej);
	})
};
