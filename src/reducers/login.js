import actionTypes from '../constants/ActionTypes';

export default (state = {
	id: 1,
	login: '',
	role: 'unauthorized user',
}, action) => {
	let newState = {};
	switch (action.type) {
		case actionTypes.SIGN_UP:
		case actionTypes.LOG_IN:
			newState = Object.assign({}, state);
			newState.id = 1;
			newState.login = action.payload.name;
			return newState;
		case actionTypes.LOG_OUT:
			newState = Object.assign({}, state);
			newState.id = 1;
			newState.login = action.payload;
			return newState;
		default:
			return state;
	}
}
