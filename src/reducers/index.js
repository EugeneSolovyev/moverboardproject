import {
	createStore,
	applyMiddleware
} from 'redux';
import thunk from 'redux-thunk';
import login from './login';
import profile from './profile';

function mainReducer(store = {}, action) {
	return {
		login: login(store.login, action),
		profile: profile(store.profile, action)
	}
}

const Store = createStore(
	mainReducer,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
	applyMiddleware(thunk),
);

export default Store;
