import actionTypes from "../constants/ActionTypes";

export default (state = {
	login: "",
	googleID: "",
	birthday: "",
	firstName: "",
	lastName: "",
	licenseNumber: "",
	additionalInfo: "",
	photoUrl: "",
	role: "unauthorized user"
}, action) => {
	
	switch (action.type) {
		case actionTypes.GET_PROFILE:
			return action.payload;
		case actionTypes.SAVE_PROFILE:
			return action.payload;
		default:
			return state;
	}
}

