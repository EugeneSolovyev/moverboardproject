import "./normalize.css";

import "simple-line-icons/fonts/Simple-Line-Icons.svg"
import "simple-line-icons/fonts/Simple-Line-Icons.woff2"
import "simple-line-icons/fonts/Simple-Line-Icons.woff"
import "simple-line-icons/fonts/Simple-Line-Icons.ttf"
import "simple-line-icons/fonts/Simple-Line-Icons.eot"
import "simple-line-icons/css/simple-line-icons.css"

import "react-md/dist/react-md.deep_purple-pink.min.css";

window.moment = require("moment");
require("imports-loader?computedStyle=computed-style!computed-style");
require("imports-loader?_=lodash!lodash");
require('snapsvg');