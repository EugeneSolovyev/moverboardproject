import React from "react";
import {Button, TextField} from "react-md";
import Calendar from "react-calendar";
import "./profile.style.scss";

import ImageLoaderComponent from "./imageLoader/imageLoader.component";

const FORMAT_DATE = 'DD.MM.YYYY';

export default class ProfileComponent extends React.Component {
	
	constructor(props) {
		super(props);
		
		this.toggleCalendar = this.toggleCalendar.bind(this);
		this.saveProfile = this.saveProfile.bind(this);
		this.selectDayCalendar = this.selectDayCalendar.bind(this);
		this.selectDayInput = this.selectDayInput.bind(this);
		this.changeField = this.changeField.bind(this);
		this.validateFields = this.validateFields.bind(this);

		this.state = this.props.profile;
		this.state.birthdayDate = undefined;
		this.state.calendarOpened = false;
		this.state.saving = false;
	}
	
	toggleCalendar(e) {
		e.stopPropagation();
		this.setState({
			calendarOpened: !this.state.calendarOpened
		})
	}
	
	selectDayCalendar(date) {
		this.setState({
			birthdayDate: date,
			birthday: moment(date).format(FORMAT_DATE),
			calendarOpened: false,
		});
	}
	
	selectDayInput(dateString) {
		let stateChange = {};
		stateChange.birthday = dateString;
		stateChange.calendarOpened = false;
		let date = moment(dateString, FORMAT_DATE, true);
		if (date.isValid()) {
			stateChange.birthdayDate = date._d;
		}
		this.setState(stateChange);
	}
	
	changeField(value, fieldName) {
		this.setState({
			[fieldName]: value
		});
	}
	
	validateFields() {
		return true;
	}
	
	saveProfile() {
		if (this.validateFields()) {
			this.setState({
				saving: true
			});
			let profile = {
				googleID: this.state.googleID || "",
				birthday: this.state.birthday || "",
				firstName: this.state.firstName || "",
				lastName: this.state.lastName || "",
				licenseNumber: this.state.licenseNumber || "",
				additionalInfo: this.state.additionalInfo || "",
				photoUrl: this.state.photoUrl || "",
			};
			this.props.saveProfile(profile, result => {
				console.log(result);
				this.setState({
					saving: false
				});
			});
		}
	}
	
	render() {
		let calendarClass = this.state.calendarOpened ? "opened" : "hidden";
		
		return (
			<div className="profile">

				<ImageLoaderComponent
				/>
				
				<TextField
					id="profile-first-name"
					label="First Name"
					lineDirection="center"
					fullWidth={true}
					value={this.state.firstName}
					onChange={(value) => {this.changeField(value, "firstName")}}
				/>
				
				<TextField
					id="profile-last-name"
					label="Last Name"
					lineDirection="center"
					fullWidth={true}
					value={this.state.lastName}
					onChange={(value) => {this.changeField(value, "lastName")}}
				/>
				
				<TextField
					id="profile-license-number"
					label="License Number"
					lineDirection="center"
					fullWidth={true}
					value={this.state.licenseNumber}
					onChange={(value) => {this.changeField(value, "licenseNumber")}}
				/>
				
				<div
					className="calendarInput"
				>
					<TextField
						id="profile-birthday"
						label="Birthday"
						fullWidth={true}
						value={this.state.birthday}
						onChange={this.selectDayInput}
						inlineIndicator={
							<div
								onClick={this.toggleCalendar}>
								<i className="icon-calendar"/>
							</div>
						}
					/>
					<Calendar
						className={`calendar calendar-${calendarClass}`}
						value={this.state.birthdayDate}
						onChange={this.selectDayCalendar}
						activeStartDate={new Date(1980, 0, 1)}
					/>
				</div>
				
				<TextField
					id="profile-additional"
					label="Additional Info"
					lineDirection="center"
					fullWidth={true}
					maxRows={10}
					value={this.state.additionalInfo}
					onChange={(value) => {this.changeField(value, "additionalInfo")}}
				/>
				
				<Button
					raised
					onClick={this.saveProfile}
				>
					Save & Create Advertisement
				</Button>
			
			</div>
		)
	}
}
