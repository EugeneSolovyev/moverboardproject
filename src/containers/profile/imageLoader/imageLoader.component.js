import React from "react";
import animationClass from "./waveAnimation"
import computedStyle from "computed-style";

export default class ImageLoaderComponent extends React.Component {

	constructor(props) {
		super(props);

		this.drawAnimation = this.drawAnimation.bind(this);
	}

	componentDidMount() {
		let animationParams = {
			snap: Snap(this.photoLoader),
			width: +computedStyle(this.photoLoader, "width").slice(0, -2),
			height: +computedStyle(this.photoLoader, "height").slice(0, -2),
			periodsCount: 4,
			duration: 2000
		};
		this.animationObject = new animationClass(animationParams);
	}

	drawAnimation() {
		this.animationObject.drawWaves();
		this.animationObject.setProgress(50);
	}

	render() {

		return (
			<div
				className="profile_image"
				onClick={this.drawAnimation}>
				<svg
					ref={(svg) => {
						this.photoLoader = svg;
					}}
					id="photoLoader"
					className="fullSizeLoader">
				</svg>
			</div>
		)
	}
}