export default class waveAnimation {
	constructor(params) {

		this.height = params.height;
		this.width = params.width;
		this.snap = params.snap;
		this.periodsCount = params.periodsCount;
		this.duration = params.duration;
		this.setProgress = this.setProgress.bind(this);
		this.getWavePath = this.getWavePath.bind(this);
		this.drawWaves = this.drawWaves.bind(this);
	}

	drawWaves() {
		let period1 = this.width / this.periodsCount;
		let waveParams1 = {
			amplitude: period1 / 3,
			period: period1,
			offsetY: 0
		};

		let period2 = period1 / 2;
		let waveParams2 = {
			amplitude: period2 / 3,
			period: period2,
			offsetY: waveParams1.amplitude / 2
		};

		let stringWave1 = this.getWavePath(waveParams1);
		let stringWave2 = this.getWavePath(waveParams2);
		this.mainLayer = this.snap.el("g")
			.append(this.snap.el("g", {"class": "imageLoader_animation_layer1"})
				.append(this.snap.path(stringWave2)
					.attr(
						{
							fill: "#cdcdcd",
							strokeWidth: 0
						}
					)
					.append(this.snap.el("animateTransform", {
						attributeName: "transform",
						type: "translate",
						from: "0",
						to: -period2,
						begin: "0s",
						dur: `${this.duration}ms`,
						repeatCount: "indefinite"
					}))))
			.append(this.snap.el("g", {"class": "imageLoader_animation_layer2"})
				.append(this.snap.path(stringWave1)
					.attr(
						{
							fill: "#878787",
							strokeWidth: 0
						}
					)
					.append(this.snap.el("animateTransform", {
						attributeName: "transform",
						type: "translate",
						from: "0",
						to: period1,
						begin: "0s",
						dur: `${this.duration}ms`,
						repeatCount: "indefinite"
					}))))
			.transform(`translate(0, ${this.height})`);
	}

	setProgress(progress) {
		let targetHeight = this.height * (100 - progress) / 100;
		this.mainLayer.animate({transform: `translate(0, ${targetHeight})`}, 1000);
	}

	getWavePath(params) {
		let startPoint = -params.period;
		let bezierTopPointY = params.amplitude * 2;
		let stopPoint = this.width + params.period;
		let halfPeriod = params.period / 2;
		let path = `M${startPoint},${-params.offsetY}`;
		for (let i = startPoint; i <= stopPoint; i += params.period) {
			path += `C${i + halfPeriod},${bezierTopPointY - params.offsetY} ${i + halfPeriod},${-bezierTopPointY - params.offsetY} ${i + params.period},${-params.offsetY}`
		}
		path += `L${stopPoint},${this.height + params.amplitude}L${startPoint},${this.height + params.amplitude}`;
		return path;
	}
}