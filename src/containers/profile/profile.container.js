import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import ProfileComponent from "./profile.component";
import {getProfile, saveProfile} from "./profile.actions";

class ProfileContainer extends React.Component {

	constructor(props) {
		super(props);

		this.getProfile = this.getProfile.bind(this);
		this.saveProfile = this.saveProfile.bind(this);

		if (this.props.profile.login == "") {
			this.getProfile();
		}
	}

	getProfile() {
		this.props.getProfile(this.props.history);
	}

	saveProfile(profile, callback) {
		this.props.saveProfile(profile, callback);
	}

	render() {
		let draftProfile = _.clone(this.props.profile);
		let profileKey = new Date().getTime();
		return (
			<div>
				<ProfileComponent key={profileKey}
										saveProfile={this.saveProfile}
				                  profile={draftProfile}/>
			</div>
		)
	}
}


const mapStateToProps = (state) => {
	return {
		profile: state.profile
	}
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		getProfile: getProfile,
		saveProfile: saveProfile
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)
