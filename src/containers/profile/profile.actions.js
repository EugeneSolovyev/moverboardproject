import axios from 'axios';
import actionTypes from '../../constants/ActionTypes'
import urles from '../../constants/urles'

export const getProfile = (history) => {
	return dispatch => axios.get(`${urles.ServerURL}/get_profile`, {
	}, urles.config).then((response) => {
		dispatch({
			type: actionTypes.GET_PROFILE,
			payload: response.data
		});
	}, rej => {
		dispatch({
			type: actionTypes.GET_PROFILE,
			payload: {
				data: {
					login: '',
					googleID: '',
					role: 'unauthorized user',
				}
			}
		});
		history.push('/');
	})
};

export const saveProfile = (profile, callback) => {
	return dispatch => axios.post(`${urles.ServerURL}/save_profile`, profile, {
	}, urles.config).then((response) => {
		if (response.data.status_code == 200) {
			dispatch({
				type: actionTypes.SAVE_PROFILE,
				payload: profile
			});
			callback(true);
		} else {
			callback(false);
			//some error handler
		}
	}, rej => {
		callback(false);
		//some error handler
	})
};
