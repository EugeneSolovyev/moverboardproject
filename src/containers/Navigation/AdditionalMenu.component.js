import React from 'react';
import PropTypes from 'prop-types';
import { MenuButton } from 'react-md';

const AdditionalMenu = ({ id, className, menuItems }) => (
	<MenuButton
		id={id}
		icon
		className={className}
		menuItems={menuItems}
	>
		<i className="icon-options-vertical"></i>
	</MenuButton>
);

AdditionalMenu.propTypes = {
	id: PropTypes.string.isRequired,
	className: PropTypes.string,
	menuItems: PropTypes.array,
};

AdditionalMenu.defaultProps = {
	menuItems: ['Account'],
};

export default AdditionalMenu;
