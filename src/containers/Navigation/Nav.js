import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-md';

const Nav = ({ className }) => {
	return (
		<Button icon
				className={className}>
			<i className="icon-menu"></i>
		</Button>
	);
};
Nav.propTypes = {
	className: PropTypes.string,
};

export default Nav;
