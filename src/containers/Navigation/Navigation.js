import React from 'react';
import { Toolbar } from 'react-md';
import Nav from './Nav';
import AdditionalMenu from './AdditionalMenu.component';

import './Navigation.scss';

class Navigation extends React.Component {
	render () {
		return (
			<div className="navigation-panel">
				<Toolbar
					themed
					nav={<Nav />}
					title="MoverBoard"
					actions={
						<AdditionalMenu id="toolbar-transparent-kebab-menu" />
					}
				/>
			</div>
		);
	}
}

export default Navigation;
