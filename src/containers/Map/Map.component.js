import React from 'react';
import {
	ComposableMap,
	ZoomableGroup,
	Geographies,
	Geography,
} from 'react-simple-maps';
import {
	Button,
	FontIcon
} from 'react-md';
import * as topojson from "./topo-map.json";

class MapComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			zoom: 1,
		};
		this.handleZoomIn = this.handleZoomIn.bind(this);
		this.handleZoomOut = this.handleZoomOut.bind(this);
	}
	
	handleZoomIn () {
		this.setState({
			zoom: this.state.zoom * 2
		})
	}
	
	handleZoomOut () {
		if (this.state.zoom < 1) return;
		
		this.setState({
			zoom: this.state.zoom / 2
		})
	}
	
	render() {
		return(
			<div>
				<Button icon
						mini
						onClick={ this.handleZoomIn }>
					<i className="icon-plus"></i>
				</Button>
				<Button icon
						mini
						onClick={ this.handleZoomOut }>
					<i className="icon-minus"></i>
				</Button>
				<ComposableMap
					projectionConfig={{
					scale: 200,
					rotation: [-10,0,0],
				}}>
					<ZoomableGroup zoom={ this.state.zoom }>
						<Geographies geography={ topojson }>
							{(geographies, projection) => geographies.map(geography => (
								<Geography
									style={{
										default: {fill: '#bdc3c7'},
										hover: {fill: '#34495e'},
										pressed: {fill: '#2c3e50'},
										outline: 'none',
									}}
									key={ geography.id }
									geography={ geography }
									projection={ projection }
								/>
							))}
						</Geographies>
					</ZoomableGroup>
				</ComposableMap>
			</div>
		);
	}
}

export default MapComponent;
