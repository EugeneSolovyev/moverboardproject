import React from 'react';
import {
	Avatar,
	Button,
	Card,
	CardActions,
	CardText,
	CardTitle,
	Media,
	MediaOverlay,
} from 'react-md';

import './advertisement.scss';

class AdvertisementComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = this.props.advert;
	}
	
	render () {
		return (
			<div className="advertisement">
				<Card>
					<CardTitle title={this.state.user.name}
								subtitle={`Company: ${this.state.user.company}`}
								avatar={<Avatar src={this.state.user.avatar}/>} />
					<Media>
						<img src={this.state.transportation.advertImage}
							 alt="example"/>
						<MediaOverlay>
							<CardTitle title={`${this.state.transportation.from} - ${this.state.transportation.to}`}
									   subtitle={this.state.transportation.date} />
						</MediaOverlay>
					</Media>
					<CardText>
						{this.state.additionalText}
					</CardText>
					<CardActions>
						<Button flat>Contact the driver</Button>
					</CardActions>
				</Card>
			</div>
		);
	}
}

export default AdvertisementComponent;
