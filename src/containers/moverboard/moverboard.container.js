import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import MoverboardComponent from "./moverboard.component";

import './moverboard.scss';

class MoverboardContainer extends Component {
	constructor(props) {
		super(props);
	}
	
	render() {
		return (
			<div className="mover-board-container">
				<MoverboardComponent />
			</div>
		)
	}
}


const mapStateToProps = (state) => {
	return {
		varInStore: state
	}
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
	
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(MoverboardContainer)
