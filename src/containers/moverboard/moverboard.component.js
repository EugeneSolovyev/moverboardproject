import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Cell } from 'react-md';

import { ADVERT_ARRAY } from '../../../mocks/advert-array';

import AdvertisementComponent from '../advertisement/Advertisement.component';
import MapComponent from '../Map/Map.component';

export default class MoverboardComponent extends React.Component {
	
	constructor(props) {
		// set defaults
		super(props);
	}
	
	render() {
		return (
			<div className="moverboard">
				<Grid>
					<Cell size={6}
						  className="Advert">
						{ADVERT_ARRAY.map(advert => {
							return (
								<div key={advert.key}>
									<AdvertisementComponent advert={advert} />
								</div>
							)
						})}
					</Cell>
					<Cell size={6}
						  className="map-wrapper">
						<MapComponent />
					</Cell>
				</Grid>
				{/*<Link to="/auth">*/}
					{/*<RaisedButton label="GO!" />*/}
				{/*</Link>*/}
			</div>
		)
	}
}
