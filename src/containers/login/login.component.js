import React from "react";
import _ from 'lodash';

import {
	RaisedButton,
	TextField,
	RadioButton,
	RadioButtonGroup
} from "material-ui";

import "./login.scss";

class LoginComponent extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			login: '',
			password: '',
			role: 'unauthorized user',
		};
		
		this.onClickSignUp = this.onClickSignUp.bind(this);
		this.onClickLogin = this.onClickLogin.bind(this);
		this.changeLoginField = this.changeLoginField.bind(this);
		this.changePasswordField = this.changePasswordField.bind(this);
		this.onClickGoogle = this.onClickGoogle.bind(this);
		this.onChangeRole = this.onChangeRole.bind(this);
	}
	
	onClickGoogle() {
		this.props.GoogleLogIn(this.state);
	}
	
	onClickSignUp() {
		this.props.SignUp(this.state);
	}
	
	onClickLogin() {
		this.props.LogInAction(this.state);
	}
	
	componentDidMount() {
		// Do async request for check login
	}
	
	changeLoginField(e) {
		this.setState({
			login: e.target.value
		})
	}
	
	changePasswordField(e) {
		this.setState({
			password: e.target.value
		})
	}
	
	onChangeRole (e, role) {
		this.setState({
			role: role
		})
	}
	
	render() {
		return (
			<div className="LoginRoot">
				<div className="LoginBox">
					<div className="UserBox">
						<TextField hintText="Login"
								   type="text"
								   value={this.state.login}
								   onChange={this.changeLoginField}/>
						<TextField hintText="Password"
								   type="password"
								   value={this.state.password}
								   onChange={this.changePasswordField}/>
					</div>
					<div className="RoleBox">
						<RadioButtonGroup name="Roles"
										  defaultSelected="this.state.customer"
										  onChange={this.onChangeRole}>
							<RadioButton label="I want to relocate (I'm user)"
										 value="user"
										 labelPosition="left"/>
							<RadioButton label="I want to drive (I'm driver)"
										 value="driver"
										 labelPosition="left"/>
						</RadioButtonGroup>
					</div>
					<div className="ButtonBox">
						<RaisedButton label="Sing Up"
									  primary={true}
									  onClick={this.onClickSignUp}/>
						<RaisedButton label="Log In"
									  secondary={true}
									  onClick={this.onClickLogin}/>
						<RaisedButton label="Google"
									  secondary={true}
									  href="/auth/google"/>
						<RaisedButton label="Log Out"
									  href="/auth/logout"/>
					</div>
				</div>
				<h1>{this.state.login}</h1>
				<h1>{this.state.password}</h1>
			</div>
		);
	}
}

export default LoginComponent;
