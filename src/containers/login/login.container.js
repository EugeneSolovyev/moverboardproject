import React from 'react';
import {withRouter} from "react-router-dom";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import LoginComponent from "./login.component";

import {LogIn, SignUp, GoogleLogIn, LogOut} from './login.actions.js';

class LoginContainer extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
		};
		
		this.OnClickSignUp = this.OnClickSignUp.bind(this);
		this.OnClickLogIn = this.OnClickLogIn.bind(this);
		this.OnClickLogOut = this.OnClickLogOut.bind(this);
		this.OnClickGoogleLogIn = this.OnClickGoogleLogIn.bind(this);
	}
	
	OnClickSignUp (state) {
		this.props.SignUp(
			this.props.history,
			state
		);
	}
	
	OnClickLogIn (state) {
		this.props.LogIn(
			this.props.history,
			state
		);
	}
	
	OnClickLogOut (state) {
		this.props.LogOut(
			this.props.history,
			state
		);
	}
	
	OnClickGoogleLogIn (state) {
		this.props.GoogleLogIn(
			this.props.history,
			state
		);
	}
	
	render() {
		let login = this.props.login.login;
		let password = this.props.login.password;
		return (
			<div>
				<LoginComponent login={login}
								 password={password}
								 LogInAction={this.OnClickLogIn}
								 SignUp={this.OnClickSignUp}
								 GoogleLogIn = {this.OnClickGoogleLogIn}
								 LogOut={this.OnClickLogOut}
				/>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		login: state.login
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		LogIn: LogIn,
		SignUp: SignUp,
		GoogleLogIn: GoogleLogIn,
		LogOut: LogOut
	}, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginContainer));
