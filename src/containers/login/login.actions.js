import axios from "axios";
import actionTypes from "../../constants/ActionTypes";
import urles from "../../constants/urles";


export const GoogleLogIn = (history, loginData) => {
	return dispatch => axios.get(`${urles.ServerURL}/auth/google`, urles.config)
		.then((res) => {
			console.log("alalala");
			console.log(res);
			dispatch({
				type: actionTypes.LOG_IN,
				payload: null
			});
			history.push('/');
		}, rej => {
			console.log(rej);
		});
};

export const LogIn = (history, loginData) => {
	return dispatch => axios.post(`${urles.ServerURL}/auth/login`, loginData, urles.config)
		.then(res => {
			console.log(res);
			dispatch({
				type: actionTypes.LOG_IN,
				payload: res.data
			});
			history.push('/profile');
		}, rej => {
			console.log(rej)
		});
};

export const SignUp = (history, RegisterData) => {
	return dispatch => axios.post(`${urles.ServerURL}/auth/register`, RegisterData, urles.config)
		.then(res => {
			console.log(res);
			dispatch({
				type: actionTypes.SIGN_UP,
				payload: res.data
			});
			history.push('/profile');
		}, reject => {
			console.log(reject);
		});
};

export const LogOut = (history) => {
	return dispatch => axios.get(`${urles.ServerURL}/auth/logout`, urles.config)
		.then((res) => {
			console.log(res);
			dispatch({
				type: actionTypes.LOG_OUT,
				payload: null
			});
			history.push('/');
		}, rej => {
			console.log(rej);
		});
};
