import React from 'react';
import {
	Switch,
	Route,
} from 'react-router-dom'

import NavigationComponent from './containers/Navigation/Navigation';
import LoginContainer from "./containers/login/login.container";
import MoverboardContainer from './containers/moverboard/moverboard.container';
import ProfileContainer from './containers/profile/profile.container'

class App extends React.Component {
	
	render() {
		return (
			<div>
				<NavigationComponent/>
				<Switch>
					<Route exact path='/' component={MoverboardContainer}/>
					<Route exact path='/auth' component={LoginContainer}/>
					<Route path="/profile" component={ProfileContainer}/>
				</Switch>
			</div>
		);
	}
}

export default App;
