const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

console.log(path.join(__dirname, 'node_modules'));

module.exports = {
	
	cache: true,
	
	entry: {
		index: path.join(__dirname, 'src', 'index.js')
	},
	
	output: {
		filename: 'bundle.js',
		path: path.join(__dirname, 'build'),
		publicPath: '/'
	},
	
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			chunks: ['vendor']
		}),
		new HtmlWebpackPlugin({
			inject: true,
			template: path.join(__dirname, 'src', 'index.html'),
		}),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				unsafe: false,
				warnings: false,
				drop_console: true,
			},
			mangle: false
		})
	],
	
	resolve: {
		extensions: ['.js', '.jsx', 'map', 'css', 'scss'],
		modules: [path.resolve(__dirname, "node_modules")]
	},
	
	module: {
		loaders: [
			{
				test: /\.(js|jsx|mjs)$/,
				exclude: /(node_modules|bower_components)/,
				
				// "loader" property changed to "loaders" and is now an array!
				loaders: [
					
					{
						loader: 'babel-loader',
						options: {
							plugins: ["react-hot-loader/babel"],
							presets: [
								['es2015', {'modules': false}],
								'react',
								'stage-0',
								'stage-1'
							]
						}
					}
				]
			},
			{
				test: /\.(sass|scss)$/,
				use: [
					{
						loader: "style-loader" // creates style nodes from JS strings
					}, {
						loader: "css-loader" // translates CSS into CommonJS
					}, {
						loader: "sass-loader" // compiles Sass to CSS
					}
				]
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: "style-loader" // creates style nodes from JS strings
					}, {
						loader: "css-loader" // translates CSS into CommonJS
					}
				]
			},
			{
				test: /\.(eot|ttf|woff|woff2)$/,
				use: [
					{loader: 'file-loader'}
				]
			},
			{
				test: /\.svg$/,
				use: [
					{
						loader: 'url-loader'
					}
				]
			}
		]
	}
};