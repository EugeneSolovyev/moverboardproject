global.rootDir = __dirname;
global._ = require('lodash');
const colors = require('colors');
const express = require('express');
const mongoose = require( 'mongoose' );
const CORS = require('cors');

const DATABASE = require('./server/config/db-url');
const MongoStore = require('./server/api/authentication/session/session.config');

const app = express();
const PORT = 8080;
app.use(CORS());

mongoose.connect(DATABASE.url);
mongoose.connection.on('connected', next1);
function next1(err, database)  {
	
	if (err) {
		console.log(err);
	}
	
	console.log(`Database has required successfully... Server available on port ${PORT}`.green);
	
	MongoStore.session_start(app);
	require('./server/api/authentication/index')(app);
	require('./server/api/userProfile')(app);
	require('./server/api/Advertisements')(app);
	
	
	//static MUST be last
	require('./server/static/static.js')(app);
	
	// entrance point for CRUD
	//require('./server/api/db')(app, database);
	app.listen(PORT, () => {
		console.log(`Server has successfully started on port:${PORT}...`.green);
	});
	
}
